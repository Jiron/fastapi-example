import joblib
# https://github.com/koaning/scikit-lego/blob/main/sklego/datasets.py
#from palmerpenguins import load_penguins
from pkg_resources import resource_filename
from sklearn.datasets import fetch_openml

from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from lightgbm import LGBMClassifier
import tensorflow as tf
from tensorflow.estimator import BaselineClassifier
from tensorflow.estimator import DNNClassifier
from tensorflow.estimator import BoostedTreesClassifier
from tensorflow.estimator import DNNLinearCombinedClassifier
from tensorflow.estimator import LinearClassifier
import os
import pandas as pd

def train():
    data, target = load_penguins(return_X_y=True) #, as_frame=True)
    #data = data.dropna()
    #print(data.shape)
    #data, target_bid = load_bid(return_X_y=True) #, as_frame=True)
    #data, target_ask = load_ask(return_X_y=True) #, as_frame=True)

    models = {
        "rf": RandomForestClassifier(max_depth=16, random_state=0),
        "dt": DecisionTreeClassifier(max_depth=3, random_state=0),
        "lgbm": LGBMClassifier(max_depth=8, random_state=0),
        #"bl": BaselineClassifier(),
        #"dnn": DNNClassifier(feature_columns=[], hidden_units=[1024, 512, 256]),
        #"bt": BoostedTreesClassifier(max_depth=3, feature_columns=[], n_batches_per_layer=0),
        #"dnnlc": DNNLinearCombinedClassifier(linear_feature_columns=[], dnn_feature_columns=[]),
	    #"linear": LinearClassifier(feature_columns=3),
    }

    for name, model in models.items():
        model.fit(data, target)
        joblib.dump(model, f"model/{name}.pkl")

def load_penguins(return_X_y=False):#, as_frame=False):
    filepath = resource_filename("sklego", os.path.join("data", "penguins.zip"))
    df = pd.read_csv(filepath)
    df = df.dropna()
    #if as_frame:
    #    return df
    X, y = (
        df[
            [
                #"island",
                "bill_length_mm",
                "bill_depth_mm",
                "flipper_length_mm",
                "body_mass_g",
                #"sex",
            ]
        ].values,
        df["species"].values,
    )
    if return_X_y:
        return X, y
    return {"data": X, "target": y}


if __name__ == "__main__":
    #filepath = resource_filename("sklego", os.path.join("data", "penguins.zip"))
    train()
